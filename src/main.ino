#include "env.h"
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

WiFiClient espClient;
PubSubClient client(espClient);
int d2Reading = LOW;
int d1Reading = LOW;

void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WIFI_SSID);

  WiFi.begin(WIFI_SSID, WIFI_PASS);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "motionClient-";
    clientId += WiFi.macAddress();
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  pinMode(D1, INPUT);
  pinMode(D2, INPUT);
  Serial.begin(9600);
  setup_wifi();
  client.setServer(MQTT_SERVER, 1883);
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  int newD1Reading = digitalRead(D1);
  if (newD1Reading != d1Reading) {
    d1Reading = newD1Reading;
    if (d1Reading == HIGH) {
      Serial.println("Motion detected");
      client.publish(D1_TOPIC, "1");
    } else {
      client.publish(D1_TOPIC, "0");
    }
  }

  int newD2Reading = digitalRead(D2);
  if (newD2Reading != d2Reading) {
    d2Reading = newD2Reading;
    if (d2Reading == HIGH) {
      Serial.println("Motion detected");
      client.publish(D2_TOPIC, "1");
    } else {
      client.publish(D2_TOPIC, "0");
    }
  }
}
